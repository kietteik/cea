# DEPLOY HEROKU MANUAL
heroku container:login 
#   Build the image
docker build -f container/production/django/Dockerfile -t registry.heroku.com/intense-falls-72753/web .
#   Push the image to the registry
docker push registry.heroku.com/intense-falls-72753/web:latest
#   Release the image
heroku container:release web --app intense-falls-72753