from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as log_out
from django.conf import settings
from django.http import HttpResponseRedirect
from urllib.parse import urlencode

import json


def index(request):
    user = request.user
    if user.is_authenticated:
        return redirect(dashboard)
    else:
        return render(request, 'index.html')


@login_required
def dashboard(request):
    user = request.user
    CEAUuser = user.social_auth.get(provider='CEAU')
    userdata = {
        'user_id': CEAUuser.uid,
        'name': user.first_name,
        'picture': CEAUuser.extra_data['picture'],
        'email': CEAUuser.extra_data['email'],
        'scopes': CEAUuser.extra_data['scopes'],
        'roles': CEAUuser.extra_data['roles'],
    }

    return render(request, 'dashboard.html', {
        'idtoken': CEAUuser.extra_data['idtoken'],
        'accesstoken': CEAUuser.extra_data['accesstoken'],
        'CEAUUser': CEAUuser,
        'userdata': json.dumps(userdata, indent=4)
    })


def logout(request):
    log_out(request)
    return_to = urlencode({'returnTo': request.build_absolute_uri('/')})
    logout_url = 'https://%s/accounts/profile?%s' % \
                 (settings.SOCIAL_AUTH_CEAU_DOMAIN, return_to)
    return HttpResponseRedirect(logout_url)
