from urllib import request
from jose import jwt
from social_core.backends.oauth import BaseOAuth2


class CEAU(BaseOAuth2):
    """CEAU OAuth authentication backend"""
    name = 'CEAU'
    SCOPE_SEPARATOR = ' '
    ACCESS_TOKEN_METHOD = 'POST'
    REDIRECT_STATE = False
    EXTRA_DATA = [
        ('picture', 'picture'),
        ('email', 'email'),
        ('scopes', 'scopes'),
        ('roles', 'roles'),
        ('idtoken', 'idtoken'),
        ('accesstoken', 'accesstoken'),
    ]

    def authorization_url(self):
        return 'https://' + self.setting('DOMAIN') + '/api/o/authorize'

    def access_token_url(self):
        return 'https://' + self.setting('DOMAIN') + '/api/oauth/token/'

    def get_user_id(self, details, response):
        """Return current user id."""
        return details['user_id']

    def auth_headers(self):
        return {'content-type': "application/x-www-form-urlencoded",
                'cache-control': "no-cache", }

    def get_user_details(self, response):
        # Obtain JWT and the keys to validate the signature
        access_token = response.get('access_token')
        id_token = response.get('id_token')
        rq = request.Request('https://' + self.setting('DOMAIN') +
                             '/api/o/.well-known/jwks.json', headers={'User-Agent': 'Mozilla/5.0'})
        jwks = request.urlopen(rq)
        issuer = 'http://' + self.setting('DOMAIN') + '/api/o'
        audience = self.setting('KEY')  # CLIENT_ID
        payload = jwt.decode(id_token, jwks.read(), algorithms=[
                             'RS256'], audience=audience, issuer=issuer, access_token=access_token)
        return {'name': payload['name'],
                'user_id': payload['sub'],
                'email': payload['email'],
                'roles': payload['roles'],
                'scopes': payload['scopes'],
                'idtoken': id_token,
                'accesstoken': access_token,
                }
