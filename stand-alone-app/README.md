CALLBACK_URL = `http://localhost:3000/complete/CEAU/`

1. Install the needed dependencies with `pip install -r requirements.txt`
2. Run `python manage.py migrate` to migrate the database schema
3. Run `python manage.py runserver 3000` to run the server.
