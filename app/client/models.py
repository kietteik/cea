from django.db import models
from django_tenants.models import TenantMixin, DomainMixin
from client.enums import TemplateEnum
from users.models import User


class Client(TenantMixin):
    # default true, schema will be automatically created and synced when it is saved
    auto_create_schema = True
    auto_drop_schema = True

    class Meta:
        ordering = ["-created_on"]

    name = models.CharField(max_length=100)
    created_on = models.DateField(auto_now_add=True)
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, blank=True, null=True)

    # tentan settings fields
    login_template = models.CharField(
        choices=TemplateEnum.choices(), blank=True, null=True, default=1, max_length=10)
    opt_email_login_option = models.BooleanField(default=False)
    otp_phone_login_option = models.BooleanField(default=False)
    forgot_password_option = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Domain(DomainMixin):
    class Meta:
        ordering = ["tenant"]

    def __str__(self):
        return self.domain
