# Generated by Django 3.2.9 on 2021-12-21 12:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0004_auto_20211130_1255'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='login_template',
            field=models.CharField(blank=True, choices=[('1', 'Generic'), ('2', 'Basic'), ('3', 'Advance')], default=1, max_length=10, null=True),
        ),
    ]
