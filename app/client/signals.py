from django.dispatch import Signal, receiver
from django_tenants.signals import post_schema_sync
from django_tenants.models import TenantMixin
from django_tenants.utils import schema_context, schema_exists
from django.db.models.signals import post_delete
from users.models import User
from .models import Client


@receiver(post_schema_sync, sender=TenantMixin)
def created_user_client(sender, **kwargs):
    print("SIGNAL CREATE TENANT CALLED !!")
    print("CREATING SRUPERUSER FOR TENANT...")
    client = kwargs['tenant']
    tenant_name = client.name
    print(client.name)

    with schema_context(tenant_name):
        if not User.objects.filter(email=f"{tenant_name}@admin.vn").exists():
            User.objects.create_superuser(f"{tenant_name}@admin.vn", "admin")
            print(f"CREATED SUPRERUSER FOR {tenant_name} DOMAIN")
        else:
            print(f"SKIP CREATE SUPRERUSER FOR {tenant_name} DOMAIN")
