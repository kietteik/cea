from enum import Enum


class TemplateEnum(str, Enum):
    Generic = 1
    Basic = 2
    Advance = 3

    @classmethod
    def choices(cls):
        return tuple((i.value, i.name) for i in cls)
