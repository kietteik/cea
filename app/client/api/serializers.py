from client.models import Client, Domain
from rest_framework import serializers


class DomainSerializer(serializers.ModelSerializer):
    tenant_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Domain
        fields = [
            "id",
            "domain",
            "is_primary",
            "tenant_id",
        ]


class ClientSerializer(serializers.ModelSerializer):
    domains = DomainSerializer(many=True, read_only=True)

    class Meta:
        model = Client
        fields = [
            "id",
            "name",
            "schema_name",
            "created_on",
            "login_template",
            "opt_email_login_option",
            "otp_phone_login_option",
            "forgot_password_option",
            "domains",
            "owner",
        ]
