from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from .views import (
    ClientDetailView,
    ClientView,
    DomainDetailView,
    DomainView,
)

urlpatterns = [
    path("", ClientView.as_view(), name="client"),
    path("domain/", DomainView.as_view(), name="domain"),
    path("<pk>/", ClientDetailView.as_view(), name="client-detail"),
    path("domain/<pk>/", DomainDetailView.as_view(), name="domain-detail"),
]
