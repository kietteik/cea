from client.models import Domain
from rest_framework import filters, generics, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView, Response
from client.models import Client
from .serializers import ClientSerializer, DomainSerializer


class ClientView(generics.ListCreateAPIView):
    """Create/list clients for current user"""

    serializer_class = ClientSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [filters.SearchFilter, filters.OrderingFilter, ]
    search_fields = ["email", "phone", "name"]
    ordering_fields = "__all__"
    queryset = Client.objects.all()

    def get_queryset(self):
        return Client.objects.filter(owner=self.request.user) if not self.request.user.is_anonymous else None

    def create(self, request, *args, **kwargs):
        self.request.data["owner"] = request.user.id
        return super().create(request, *args, **kwargs)


class ClientDetailView(generics.RetrieveUpdateDestroyAPIView):
    """Detail clients for current user"""
    http_method_names = ['get', 'patch', 'delete']
    serializer_class = ClientSerializer
    permission_classes = [IsAuthenticated]
    queryset = Client.objects.all()

    def get_queryset(self):
        return Client.objects.filter(owner=self.request.user) if not self.request.user.is_anonymous else None

    def update(self, request, *args, **kwargs):
        print(request.data)
        return super().update(request, *args, **kwargs)


class DomainView(generics.ListCreateAPIView):
    """Create/list domain for current user"""

    queryset = Domain.objects.all()
    serializer_class = DomainSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [
        filters.SearchFilter,
        filters.OrderingFilter,
    ]
    ordering_fields = "__all__"

    def get_queryset(self):
        return Domain.objects.filter(tenant__owner=self.request.user) if not self.request.user.is_anonymous else None


class DomainDetailView(generics.RetrieveUpdateDestroyAPIView):
    """Detail domain for current user"""
    http_method_names = ['get', 'patch', 'delete']
    serializer_class = DomainSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Domain.objects.filter(tenant__owner=self.request.user) if not self.request.user.is_anonymous else None
