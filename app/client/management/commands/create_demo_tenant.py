from django.core.management.base import BaseCommand
from client.models import Client, Domain
from django_tenants.utils import schema_context
from users.models import User
from config import settings


class Command(BaseCommand):
    """Django command to pause execution until database is available"""

    domain_name = settings.DOMAIN_NAME

    def handle(self, *args, **options):

        public, created = Client.objects.get_or_create(
            schema_name="public",
            defaults={
                "schema_name": "public",
                "name": "Cea Services Main Page",
            },
        )

        public_domain, created = Domain.objects.get_or_create(
            domain=self.domain_name,
            defaults={
                "domain": self.domain_name,
                "tenant": public,
                "is_primary": True,
            },
        )

        demo, created = Client.objects.get_or_create(
            schema_name="demo",
            defaults={
                "schema_name": "demo",
                "name": "demo page",
            },
        )

        # Add one or more domains for the tenant
        demo_domain, created = Domain.objects.get_or_create(
            domain="demo." + self.domain_name,
            defaults={
                "domain": "demo." + self.domain_name,
                "tenant": demo,
                "is_primary": True,
            },
        )

        with schema_context("demo"):
            if not User.objects.filter(email="demo@demo.vn").exists():
                User.objects.create_superuser("demo@demo.vn", "admin")
                print("CREATED SUPRERUSER FOR demo DOMAIN")
            else:
                print("SKIP CREATE SUPRERUSER FOR demo DOMAIN")
