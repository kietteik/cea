
from rest_framework import serializers

from tenant.models import TenantAPI, TenantApplication, TenantRole
from users.api.serializers import UserSerializer


class ApplicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = TenantApplication
        fields = "__all__"


class TenantAPISerializer(serializers.ModelSerializer):
    class Meta:
        model = TenantAPI
        fields = "__all__"


class TenantRoleWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = TenantRole
        fields = "__all__"
        extra_kwargs = {'users': {'required': False},
                        'permissions': {'required': False}}


class TenantRoleReadSerializer(serializers.ModelSerializer):
    users = UserSerializer(many=True, read_only=True)
    permissions = TenantAPISerializer(many=True, read_only=True)

    class Meta:
        model = TenantRole
        fields = "__all__"
        extra_kwargs = {'users': {'required': False},
                        'permissions': {'required': False}}


class TenantAPIReadSerializer(serializers.ModelSerializer):

    class Meta:
        model = TenantAPI
        fields = "__all__"
