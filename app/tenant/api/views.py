from rest_framework.views import APIView
from config.permission import IsTenantOwner
from tenant.models import TenantAPI, TenantApplication, TenantRole
from tenant_manage.api.views import TenantProvidingMixin
from users.api.serializers import UserSerializer
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from users.models import User
from config.middleware import PublicJWTAuthentication
from tenant.api.serializers import ApplicationSerializer, TenantAPISerializer, TenantRoleReadSerializer, TenantRoleWriteSerializer
from django.shortcuts import render
from rest_framework import generics, status, viewsets
from oauth2_provider.contrib.rest_framework import OAuth2Authentication
from oauth2_provider.views.mixins import OAuthLibMixin
from oauth2_provider.models import get_access_token_model
from oauth2_provider.signals import app_authorized
from django.http import HttpResponse, JsonResponse
import json
from django.utils.decorators import method_decorator
from django.views.decorators.debug import sensitive_post_parameters

# Create your views here.


class ApplicationListView(generics.ListCreateAPIView):
    serializer_class = ApplicationSerializer
    queryset = TenantApplication.objects.all()
    authentication_classes = [PublicJWTAuthentication]
    permission_classes = [IsTenantOwner]

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class ApplicationDetailView(generics.RetrieveUpdateDestroyAPIView):
    http_method_names = ['get', 'patch', 'delete']
    serializer_class = ApplicationSerializer
    queryset = TenantApplication.objects.all()
    authentication_classes = [PublicJWTAuthentication]
    permission_classes = [IsTenantOwner]


class TokenView(OAuthLibMixin, APIView):
    """
    Implements an endpoint to provide access tokens

    The endpoint is used in the following flows:
    * Authorization code
    * Password
    * Client credentials
    """
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        url, headers, body, status = self.create_token_response(request)
        if status == 200:
            access_token = json.loads(body).get("access_token")
            body = json.loads(body)
            if access_token is not None:
                token = get_access_token_model().objects.get(token=access_token)
                app_authorized.send(sender=self, request=request, token=token)
                body = json.dumps(body)
        response = HttpResponse(content=body, status=status)

        for k, v in headers.items():
            response[k] = v
        return response


class TenantRoleViewSet(viewsets.ModelViewSet):
    """
    List all roles, or create a new role.
    """
    queryset = TenantRole.objects.all()

    def get_serializer_class(self, *args, **kwargs):
        if self.action in ['list', 'retrieve']:
            return TenantRoleReadSerializer
        else:
            return TenantRoleWriteSerializer


class TenantAPIViewSet(viewsets.ModelViewSet):
    """
    List all api, or create a new api.
    """
    queryset = TenantAPI.objects.all()

    def get_serializer_class(self, *args, **kwargs):
        return TenantAPISerializer


class GenerateSystemPermission(APIView):
    permission_classes = [AllowAny]

    def get(self, request, *args, **kwargs):
        permissions = TenantProvidingMixin.tenant_permissions
        objs = [
            TenantAPI.objects.get_or_create(name=perm, defaults={"name": perm})
            for perm in permissions
        ]
        return Response(status=status.HTTP_201_CREATED)
