from .views import ApplicationListView, TenantAPIViewSet, TenantRoleViewSet, TokenView, ApplicationDetailView, GenerateSystemPermission
from django.urls import path, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'roles', TenantRoleViewSet, basename='role')
router.register(r'tenant-apis', TenantAPIViewSet, basename='api')

urlpatterns = [
    path("applications/", ApplicationListView.as_view(), name="application-list"),
    path("applications/<pk>/", ApplicationDetailView.as_view(),
         name="application-detail"),
    path("oauth/token/", TokenView.as_view(), name="oauth-token"),
    path("permission/generate/", GenerateSystemPermission.as_view()),
    path(r'', include(router.urls)),
]
