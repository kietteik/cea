from enum import Enum


class ApiType(str, Enum):
    System = "System"
    User = "User"

    @classmethod
    def choices(cls):
        return tuple((i.value, i.name) for i in cls)
