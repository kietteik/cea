from oauth2_provider.scopes import BaseScopes
from oauthlib.common import Request
from tenant.models import TenantAPI


class CustomScope(BaseScopes):

    def get_all_scopes(self):

        objs = TenantAPI.objects.all()
        scope = {obj.name: f"{obj.name} | {obj.type} scope" for obj in objs}
        scope.update({"openid": "OpenID Connect scope",
                      "introspection": "Introspect token scope", })
        return scope

    def get_available_scopes(self, application=None, request=None, *args, **kwargs):
        app_scope = [x.__str__() for x in application.scopes.all()]
        user_scope = [x.__str__() for x in TenantAPI.objects.filter(
            roles__users=request.user).distinct()] if request.user is not None else []

        return list(set(app_scope+user_scope))+["openid", "instrospection"]

    def get_default_scopes(self, application=None, request: Request = None, *args, **kwargs):
        app_scope = [x.__str__() for x in application.scopes.all()]
        user_scope = [x.__str__() for x in TenantAPI.objects.filter(
            roles__users=request.user).distinct()] if request.user is not None else []
        scope = app_scope+user_scope
        if application.authorization_grant_type == "authorization-code":
            scope += ["openid"]

        return list(set(scope))
