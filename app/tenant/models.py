from oauth2_provider.settings import oauth2_settings
from django.db import models
from django.db.models.fields.related import ManyToManyField
# Create your models here.
from oauth2_provider.models import AbstractApplication, AbstractAccessToken
import uuid
from tenant.enums import ApiType
from users.models import User
from django.test.signals import setting_changed


class TenantAPI(models.Model):

    class Meta:
        verbose_name = 'Tenant permission'
        verbose_name_plural = 'Tenant permissions'

    def __str__(self):
        return self.name

    id = models.BigAutoField(primary_key=True)
    url = models.CharField(max_length=255)
    name = models.CharField(max_length=255, unique=True)
    type = models.CharField(choices=ApiType.choices(),
                            default=ApiType.System, max_length=255)


class TenantApplication(AbstractApplication):
    type = models.CharField(max_length=255, blank=True, null=True)
    scopes = ManyToManyField(
        TenantAPI, related_name='applications', blank=True)


class TenantRole(models.Model):
    def __str__(self):
        return self.name

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255, blank=True, null=True)
    users = ManyToManyField(User, related_name='roles', blank=True)
    permissions = ManyToManyField(
        TenantAPI, related_name='roles', blank=True)
