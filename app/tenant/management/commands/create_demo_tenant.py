from django.core.management.base import BaseCommand
from client.models import Client, Domain
from django_tenants.utils import schema_context
from users.models import User
from config import settings


# class Command(BaseCommand):
#     """Django command to pause execution until database is available"""

#     def handle(self, *args, **options):
