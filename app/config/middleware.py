from django.db import connection
from django_tenants.middleware.main import TenantMainMiddleware
from django_tenants.utils import (
    get_tenant_domain_model,
    schema_context,
)
from rest_framework_simplejwt.authentication import JWTAuthentication


class CustomTenantMiddleware(TenantMainMiddleware):
    def process_request(self, request):
        # Connection needs first to be at the public schema, as this is where
        # the tenant metadata is stored.

        connection.set_schema_to_public()
        hostname = self.hostname_from_request(request)

        domain_model = get_tenant_domain_model()
        try:
            tenant = self.get_tenant(domain_model, hostname)
            if tenant.schema_name == "public":
                tenant.domain_url = hostname
                request.tenant = tenant
                connection.set_tenant(request.tenant)
                self.setup_url_routing(request=request, force_public=True)
                return
        except domain_model.DoesNotExist:
            self.no_tenant_found(request, hostname)
            return

        tenant.domain_url = hostname
        request.tenant = tenant
        connection.set_tenant(request.tenant)
        self.setup_url_routing(request)


class PublicJWTAuthentication(JWTAuthentication):
    def authenticate(self, request):
        with schema_context("public"):
            header = self.get_header(request)
            if header is None:
                return None

            raw_token = self.get_raw_token(header)
            if raw_token is None:
                return None

            validated_token = self.get_validated_token(raw_token)

            return self.get_user(validated_token), validated_token
