from typing import Dict
from django.http.request import HttpRequest
from oauth2_provider.oauth2_backends import OAuthLibCore
from oauth2_provider.oauth2_validators import OAuth2Validator
import json
from urllib.parse import parse_qs

from tenant.models import TenantAPI


class JSONOAuthLibCore(OAuthLibCore):
    """
    Extends the default OAuthLibCore to parse correctly application/json requests
    """

    def _get_extra_credentials(self, request: HttpRequest):
        """
        Produce extra credentials for token response. This dictionary will be
        merged with the response.
        See also: `oauthlib.oauth2.rfc6749.TokenEndpoint.create_token_response`

        :param request: The current django.http.HttpRequest object
        :return: dictionary of extra credentials or None (default)
        """
        body = dict(self.extract_body(request))
        extra = {
            'aud': None,
            'iss': request.build_absolute_uri().replace(request.get_full_path(), '/')
        }
        if 'audience' in body:
            extra['aud'] = body['audience']

        return extra

    def extract_body(self, request):
        """
        Extracts the JSON body from the Django request object
        :param request: The current django.http.HttpRequest object
        :return: provided POST parameters "urlencodable"
        """
        if parse_qs(request.body.decode("utf-8")):
            return {k: v[0] for k, v in parse_qs(request.body.decode("utf-8")).items()}.items()

        try:
            body = json.loads(request.body.decode("utf-8")).items()
        except AttributeError:
            body = ""
        except ValueError:
            body = ""

        return body


class CustomOAuth2Validator(OAuth2Validator):

    def get_additional_claims(self, request):
        user_scopes = TenantAPI.objects.filter(
            roles__users=request.user).distinct()
        roles = " ".join([x.__str__() for x in request.user.roles.all()])
        scopes = " ".join([x.__str__() for x in user_scopes])
        return {
            "email": request.user.email,
            "name": request.user.name,
            "last_name": request.user.last_name,
            "scopes": scopes,
            "roles": roles
        }
