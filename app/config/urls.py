"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from django.urls import include, path
from rest_framework import permissions
import os
from django.conf.urls.static import static
from django.conf import settings
from rest_framework_simplejwt import views as jwt_views
from oauth2_provider import urls as oauth2_provider_urls
from django.contrib.auth import urls as auth_urls

from users.views import CustomLoginView, LoginWithOTPView, ProfileView, SendOTPView, SignUpView, CustomLogoutView

schema_view = get_schema_view(
    openapi.Info(
        title="Cea API",
        default_version="v1",
        description="Cea API",
        contact=openapi.Contact(email="noreply@gmail.com"),
        license=openapi.License(name="API License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path(
        "docs/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path("admin/", admin.site.urls),
    path("api/users/", include("users.api.urls")),
    path("api/client/", include("client.api.urls")),
    path("api/o/", include(oauth2_provider_urls), name="oauth2_provider"),
    path("api/", include("tenant.api.urls")),
    path("api/tenant/", include("tenant_manage.api.urls")),
    path("accounts/signup/", SignUpView.as_view(), name="signup"),
    path("accounts/profile/", ProfileView.as_view(), name="profile"),
    path("accounts/login/", CustomLoginView.as_view(), name="login"),
    path("accounts/logout/", CustomLogoutView.as_view(), name="logout"),
    path("accounts/login/otp/verify/",
         LoginWithOTPView.as_view(), name="login-otp"),
    path("accounts/login/otp/send/", SendOTPView.as_view(), name="send-otp"),
    path("accounts/", include(auth_urls)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
