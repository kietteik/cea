from rest_framework.permissions import BasePermission
from django_tenants.utils import schema_context


class IsTenantOwner(BasePermission):
    def has_permission(self, request, view):
        with schema_context("public"):
            return request.tenant.owner == request.user
