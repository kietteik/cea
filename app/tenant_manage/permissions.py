from rest_framework.permissions import BasePermission
from django_tenants.utils import schema_context


class UserHasPermission(BasePermission):
    def has_permission(self, request, view):
        print(request.auth.scope)
        print(view.get_user_permission_lookup())
        return True
