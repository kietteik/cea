from rest_framework.exceptions import MethodNotAllowed
from tenant.models import TenantRole
from tenant_manage.permissions import UserHasPermission
from users.api.serializers import UserSerializer
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from oauth2_provider.contrib.rest_framework import OAuth2Authentication
from oauth2_provider.signals import app_authorized
from django.http import HttpResponse, JsonResponse
import json
from rest_framework import generics, status

from users.models import User
# Create your views here.


class TenantProvidingMixin:
    tenant_permissions = [
        'read_profile',
        'read_user',
        'write_user',
        'change_user',
        'remove_user',
    ]

    user_permission_lookup = None

    def get_user_permission_lookup(self):
        if self.user_permission_lookup is None:
            raise NotImplementedError("user_permission_lookup must be defined")
        if self.request.method == "GET":
            return 'read_' + self.user_permission_lookup
        elif self.request.method == "POST":
            return 'write_' + self.user_permission_lookup
        elif self.request.method == "PATCH":
            return 'change_' + self.user_permission_lookup
        elif self.request.method == "DELETE":
            return 'remove_' + self.user_permission_lookup
        else:
            raise MethodNotAllowed()


class TenantUserProfile(TenantProvidingMixin, generics.GenericAPIView):
    """
    Provide api to get logged in user's profile
    Permission: read_profile
    """

    user_permission_lookup = 'profile'
    permission_classes = [UserHasPermission]
    authentication_classes = [OAuth2Authentication]
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def get(self, request, *args, **kwargs):
        return Response(UserSerializer(request.user).data, status=status.HTTP_200_OK)


class TenantUserListView(TenantProvidingMixin, generics.ListCreateAPIView):
    """
    Provide api for who has permission to read list and create user data
    Permission: read_user, write_user
    """

    user_permission_lookup = 'user'
    authentication_classes = [OAuth2Authentication]
    permission_classes = [UserHasPermission]
    serializer_class = UserSerializer
    queryset = User.objects.all()


class TenantUserDetailView(TenantProvidingMixin, generics.RetrieveUpdateDestroyAPIView):
    """
    Provide api for admin or who has permission to access List user data
    Permission: read_user, change_user, remove_user
    """

    http_method_names = ['get', 'patch', 'delete']
    user_permission_lookup = 'user'
    authentication_classes = [OAuth2Authentication]
    permission_classes = [UserHasPermission]
    serializer_class = UserSerializer
    queryset = User.objects.all()


class TenantRoleListView(TenantProvidingMixin, generics.ListCreateAPIView):
    """
    Provide api for who has permission to read list and create user data
    Permission: read_user, write_user
    """

    user_permission_lookup = 'role'
    authentication_classes = [OAuth2Authentication]
    permission_classes = [UserHasPermission]
    serializer_class = UserSerializer
    queryset = TenantRole.objects.all()


class TenantRoleDetailView(TenantProvidingMixin, generics.RetrieveUpdateDestroyAPIView):
    """
    Provide api for admin or who has permission to access List user data
    Permission: read_user, change_user, remove_user
    """

    http_method_names = ['get', 'patch', 'delete']
    user_permission_lookup = 'role'
    authentication_classes = [OAuth2Authentication]
    permission_classes = [UserHasPermission]
    serializer_class = UserSerializer
    queryset = TenantRole.objects.all()
