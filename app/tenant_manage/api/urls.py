from . import views
from django.urls import path, include
from rest_framework.routers import DefaultRouter


urlpatterns = [
    path("me/", views.TenantUserProfile.as_view(), name="tenant-user-profile"),
    path("users/", views.TenantUserListView.as_view(), name="tenant-list-user"),
    path("users/<pk>/", views.TenantUserDetailView.as_view(),
         name="users-detail-user"),
    path("roles/", views.TenantRoleListView.as_view(), name="roles-list-user"),
    path("roles/<pk>/", views.TenantRoleDetailView.as_view(),
         name="roles-detail-user"),
]
