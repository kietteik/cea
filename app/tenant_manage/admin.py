from django.contrib import admin
from tenant.models import TenantAPI, TenantRole
# Register your models here.


@admin.register(TenantRole)
class TenantRoleAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(TenantAPI)
class TenantPermissionAdmin(admin.ModelAdmin):
    list_display = ("name",)
