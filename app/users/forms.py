from typing import Any, Dict
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.core.exceptions import ValidationError
from .models import User
from django.utils import timezone


class SignUpForm(UserCreationForm):
    email = forms.EmailField(
        max_length=200, required=True, help_text="Required")

    class Meta:
        model = User
        fields = ("username", "email")


class OTPAuthencationForm(forms.Form):
    email = forms.CharField(label="Email")
    otp = forms.CharField(label="OTP",)
    error_messages = {
        'invalid_login':
            "Please enter a correct email and OTP. Note that both "
            "fields may be case-sensitive.",
        'expired': "Your OTP is expired.",
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super().__init__(*args, **kwargs)

    def clean(self) -> Dict[str, Any]:
        email = self.cleaned_data.get('email')
        otp = self.cleaned_data.get('otp')
        if email is not None and otp:
            user = User.objects.filter(email=email).first()
            if not user or user.otp != otp:
                raise self.get_invalid_login_error()
            if timezone.now() > user.otp_expired:
                raise ValidationError(self.error_messages['expired'])
            self.user_cache = user
            login(request=self.request, user=self.user_cache)
        return self.cleaned_data

    def get_user(self):
        return self.user_cache

    def get_invalid_login_error(self):
        return ValidationError(
            self.error_messages['invalid_login'],
            code='invalid_login',
            params={'email': self.cleaned_data.get('email', '')},
        )
