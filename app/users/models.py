import uuid

from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models

# Create your models here.
from model_utils.models import TimeStampedModel


class CustomUserManager(UserManager):
    def create_user(self, email, password=None, username=None, *args, **kwargs):
        """Create and save a user"""
        if not email:
            raise ValueError("User must have an email address")
        user = self.model(email=self.normalize_email(email), **kwargs)
        user.username = username
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, password, username=None):
        """Creates and saves superuser"""
        user = self.create_user(email, password, username)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

        return user


class User(AbstractUser, TimeStampedModel):
    """Custom user model"""

    class Meta:
        ordering = ["-created"]

    objects = CustomUserManager()

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(blank=True, null=True, max_length=100)
    username = models.CharField(blank=True, null=True, max_length=100)
    email = models.EmailField(unique=True)
    phone = models.CharField(blank=True, null=True, max_length=30)
    is_online = models.BooleanField(default=False, blank=True, null=True)

    otp = models.CharField(blank=True, null=True, max_length=10)
    otp_expired = models.DateTimeField(blank=True, null=True)

    USERNAME_FIELD = "email"
    EMAIL_FIELD = "email"
    REQUIRED_FIELDS = []
