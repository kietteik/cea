from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from .views import (
    Profile,
    UserDetailView,
    UserView,
)

urlpatterns = [
    path("", UserView.as_view(), name="users"),
    path("me/", Profile.as_view(), name="user_profile"),
    path("token/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("refresh/", TokenRefreshView.as_view(), name="token_refresh"),
    path("<pk>/", UserDetailView.as_view(), name="user_detail"),
]
