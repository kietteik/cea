from rest_framework import filters, generics, status
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework.views import APIView, Response
from config.permission import IsTenantOwner

from users.models import User

from .serializers import UserSerializer


class UserView(generics.ListCreateAPIView):
    """Only Admin can read and update all users"""

    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = [
        filters.SearchFilter,
        filters.OrderingFilter,
    ]
    search_fields = ["email", "phone", "name"]
    ordering_fields = "__all__"

    def get_permissions(self):
        if self.request.method == 'GET':
            permission_classes = [IsTenantOwner | IsAdminUser]
        else:
            permission_classes = [AllowAny]
        return [permission() for permission in permission_classes]


class UserDetailView(generics.RetrieveUpdateDestroyAPIView):
    """Only Admin can read and update all users"""

    http_method_names = ["get", "patch", "delete"]
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsTenantOwner | IsAdminUser]


class Profile(APIView):
    """Get logged in user's profile"""

    permission_classes = [IsAuthenticated, ]

    def get(self, request, *args, **kwargs):
        return Response(UserSerializer(request.user).data, status=status.HTTP_200_OK)
