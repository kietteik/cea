from django.contrib.auth import get_user_model
from rest_framework import serializers
from tenant.models import TenantRole

from users.validators import PhoneValidator


class UserSerializer(serializers.ModelSerializer):
    phone = serializers.CharField(
        validators=[PhoneValidator()], allow_null=True, required=False
    )
    name = serializers.CharField(required=True)

    class Meta:
        model = get_user_model()
        fields = (
            "id",
            "username",
            "email",
            "password",
            "name",
            "phone",
            "last_login",
            "is_active",
        )
        extra_kwargs = {
            "password": {"write_only": True, "min_length": 5},
            "last_login": {"read_only": True},
        }

    def create(self, validated_data):
        return get_user_model().objects.create_user(**validated_data)

    def update(self, instance, validated_data):
        password = validated_data.pop("password", None)
        user = super().update(instance, validated_data)
        if password:
            user.set_password(password)
            user.save()
        return user


class UserDisplaySerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ("id", "email", "name")
