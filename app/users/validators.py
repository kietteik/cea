import re

from rest_framework.validators import ValidationError


class PhoneValidator(object):
    def __call__(self, phone):
        if not re.match(r"[0|+84|84]{0,4}[9|8|3|5|7]{1}[0-9]{6,10}", phone):
            raise ValidationError(
                "Phone number must be a Vietnamese phone number, include 7-15 digits. Example format: 098xxxxxxx, +8498xxxxxxx"
            )
