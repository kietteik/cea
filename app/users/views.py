from typing import Any
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.views import generic
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView, View
from .forms import OTPAuthencationForm, SignUpForm
from users.models import User
from django.contrib.auth.views import LoginView, LogoutView
from django.core.mail import send_mail
import random as rd
from datetime import timedelta
from django.utils import timezone
from django.forms.models import model_to_dict
from django import http
import json
# Create your views here.


class CustomLogoutView(LogoutView):

    def dispatch(self, request: http.HttpRequest, *args: Any, **kwargs: Any):
        context = request.GET.dict()
        if 'returnTo' in context:
            self.next_page = context['returnTo'] if context['returnTo'] else None

        return super().dispatch(request, *args, **kwargs)

    def get(self, request: http.HttpRequest, *args: Any, **kwargs: Any) -> http.HttpResponse:
        return super().get(request, *args, **kwargs)


class CustomLoginView(LoginView):
    template_dict = {
        "1": "registration/login2.html",
        "2": "registration/login.html",
        "3": "registration/login3.html",
    }
    redirect_authenticated_user = True

    def get(self, request, *args, **kwargs):
        """Handle GET requests: instantiate a blank version of the form."""

        self.extra_context = {"tenant": model_to_dict(
            request.tenant, )}
        self.template_name = self.template_dict[request.tenant.login_template]
        return self.render_to_response(self.get_context_data())

    def post(self, request: HttpRequest, *args: str, **kwargs: Any) -> HttpResponse:
        self.extra_context = {"tenant": model_to_dict(
            request.tenant, )}
        self.template_name = self.template_dict[request.tenant.login_template]
        return super().post(request, *args, **kwargs)


class SendOTPView(TemplateView):
    template_name = 'registration/send_otp.html'

    def post(self, request, *args, **kwargs):
        email = request.POST.get("email")
        user = User.objects.filter(email=email).first()
        otp = rd.randrange(1000, 9999)
        print(otp)
        if user:
            user.otp = otp
            user.otp_expired = timezone.now() + timedelta(seconds=30)
            user.save()
        send_mail(
            'CEA LOGIN OTP',
            f'Your OTP to login Cea Services is: {otp}',
            'CeaService@cea.com',
            [email, 'kietteikget@gmail.com'],
            fail_silently=False,
        )
        return HttpResponse("OPT Sent!")


class LoginWithOTPView(LoginView):
    form_class = OTPAuthencationForm
    authentication_form = None
    template_name = 'registration/login_otp_email.html'
    redirect_authenticated_user = True
    extra_context = None


class SignUpView(generic.CreateView):
    form_class = SignUpForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'


class ProfileView(TemplateView):
    template_name = 'account/profile.html'

    def get(self, request: http.HttpRequest, *args, **kwargs):
        extra_context = request.GET.dict()
        context = self.get_context_data(**kwargs, **extra_context)
        return self.render_to_response(context)
