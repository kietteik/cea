FROM python:3.8-alpine

ENV PYTHONUNBUFFERED 1


RUN apk add --update --no-cache postgresql-client jpeg-dev
RUN apk add --update --no-cache --virtual .build_deps \
  gcc libc-dev linux-headers postgresql-dev musl-dev zlib zlib-dev libffi-dev \
  # TODO workaround start
  && apk del libressl-dev \
  && apk add openssl-dev \
  && pip install --upgrade pip \
  && pip install cryptography==3.1 \
  && apk del openssl-dev 
  # TODO workaround end


# Requirements are installed here to ensure they will be cached.
COPY ./app/requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

RUN apk del .build_deps

COPY ./container/local/django/entrypoint /entrypoint
RUN sed -i 's/\r$//g' /entrypoint
RUN chmod +x /entrypoint

COPY ./container/local/django/start /start
RUN sed -i 's/\r$//g' /start
RUN chmod +x /start

RUN mkdir /app
WORKDIR /app
COPY ./app .

RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static

RUN adduser -D user
RUN chown -R user:user /vol/
RUN chmod -R 755 /vol/web

USER user

ENTRYPOINT ["sh","/entrypoint"]
